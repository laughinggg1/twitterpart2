//
//  AppDelegate.swift
//  TwitterPart2
//
//  Created by laughinggg on 2/18/19.
//  Copyright © 2019 laughinggg. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let homeController = HomeController(collectionViewLayout: UICollectionViewFlowLayout())
        
        window?.rootViewController = UINavigationController(rootViewController: homeController)
        
        
        
        return true
    }

   


}

